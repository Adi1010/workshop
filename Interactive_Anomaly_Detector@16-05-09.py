# -*- coding: utf-8 -*-

# In[1]:

import numpy as np
import scipy as sc
from scipy import stats
import matplotlib.pyplot as plt
import pandas as pd
from pandas import DataFrame, Series
import datetime


# In[2]:

df = pd.read_csv('requests.csv')


# In[40]:

df.info()
df.tail(10)


# In[4]:

#leave only informative columns
df_subset = df.filter(items=['TimeStamp', 'RoleInst', 'Continent', 'OpName', 'ReqDuration']) #'Country', 'Host' 'Response'
df_subset = df_subset.replace(np.nan, 'Null', regex=True)
#convert columns to proper type
df_subset['TimeStamp'] = df['TimeStamp'].apply(pd.to_datetime)
categorical_columns = list(set(df_subset.columns.values) - set(['TimeStamp', 'ReqDuration']))
for col in categorical_columns:
    df_subset[col] = df_subset[col].astype('category')
#subset df to contain only full data (continuous data flow stopped on 2015-10-06)
df_subset = df_subset[df_subset['TimeStamp'] < pd.to_datetime('2015-10-05')]
df_subset = df_subset.sort_values(by = 'TimeStamp')
print df_subset.dtypes


# In[29]:

print df_subset['TimeStamp'].values.astype('<M8[m]')


# In[6]:

#Group categories for OpName
tab = pd.crosstab(index=df_subset['OpName'], columns="count")
tab = tab.sort_values("count", ascending = False)
top_7_opname = list(tab.head(7).index)
df_subset['OpNameGroup'] = df_subset['OpName'].where(df_subset['OpName'].isin(top_7_opname), other = 'Other')
df_subset = df_subset.drop('OpName', axis = 1)
df_subset['OpNameGroup'] = df_subset['OpNameGroup'].astype('category')


# In[7]:

#Create combinations of Categories (Cartesian Product)
categories = []
categorical_columns = list(set(df_subset.columns.values) - set(['TimeStamp', 'ReqDuration']))
for col in categorical_columns:
    categories.append(list(df_subset[col].unique()))
print categories
#create a matrix with all combinations of column categories (each row shows a combination)
#run through each row, filter the dataframe and store the anomaly_score in the next column of the matrix


# In[84]:

import itertools
def expandgrid(*itrs):
   product = list(itertools.product(*itrs))
   return {'Var{}'.format(i+1):[x[i] for x in product] for i in range(len(itrs))}
combinations = pd.DataFrame(expandgrid(categories[0], categories[1], categories[2]))
combinations.columns = categorical_columns
print combinations


# In[85]:

def get_global_anomaly_score (values, anomaly_index, steps_back):
    ref_value = values[anomaly_index]
    prev_values = values[(max(1, anomaly_index - steps_back - 1)) : (anomaly_index - 1)]
    trim_values = stats.trimboth(prev_values, 0.1)
    trimmed_mean = np.mean(trim_values)
    trimmed_std = np.std(trim_values)
    return ref_value / trimmed_mean / trimmed_std


# In[87]:

anomaly_minute = pd.to_datetime('2015-10-04 17:06:00')
steps_back = 30
lst = []
for index, combination in combinations.iterrows():
    current_df = df_subset[((df_subset.OpNameGroup == combination['OpNameGroup']) & 
                                        (df_subset.RoleInst == combination['RoleInst']) &
                                          (df_subset.Continent == combination['Continent']))]
    current_df['TimeStamp'] = current_df['TimeStamp'].values.astype('<M8[m]')
    agg_df = current_df.groupby(['TimeStamp']).mean()
    if ((len(agg_df.index) > steps_back * 5) and not (agg_df.loc[agg_df.index == anomaly_minute].empty)):
        anomaly_index = agg_df.loc[agg_df.index == anomaly_minute]
        anomaly_score = get_global_anomaly_score(agg_df['ReqDuration'].values, anomaly_index, steps_back)
        lst.extend(combination.append(anomaly_score))



# In[ ]:

#get variable importance
#filter columns (if neccesary)


# In[ ]:

#run decision tree with good stopping criteria
#plot decision tree

